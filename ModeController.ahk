
/*
Controls the script modes
*/
class ModeController
{
	/*
	Default Constructor
	*/
	__New()
	{
		global
		
		; Initial states
		
		; 0 = FPS Mode
		; 1 = Flight Mode
		mode := 0
		
		; Hotkeys used by this controller
		Hotkey(InputKeyMap.enableFPSMode, modeController.EnableFPSMode, "")
		Hotkey(InputKeyMap.enableFlightMode, modeController.EnableFlightMode, "")
	}
	
	/*
	Enabled first person mode
	*/
	EnableFPSMode()
	{
		global
		
		mode := 0
		
		cameraController.totalCameraModes := 2
	}
	
	/*
	Enabled ship flying mode
	*/
	EnableFlightMode()
	{
		global
		
		mode := 1
		
		cameraController.totalCameraModes := 4
	}
}