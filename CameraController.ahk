
/*
Controls the game camera
*/
class CameraController
{
	/*
	Default Constructor
	*/
	__New()
	{
		global
		
		; Initial states
		cameraMode := 1
		totalCameraModes := 4
		
		; Hotkeys used by this controller
		Hotkey(InputKeyMap.changeCamera, cameraController.CycleCameraForward, "")
		Hotkey(InputKeyMap.cycleCameraForward, cameraController.CycleCameraForward, "")
		Hotkey(InputKeyMap.cycleCameraBackward, cameraController.CycleCameraBackward, "")
	}
	
	/*
	Cycles camera mode foward 
	Default camera change behavior.
	*/
	CycleCameraForward()
	{
		global
		
		; Hit the changeCamera key once
		HotKeyPassthrough(OutputKeyMap.changeCamera)
		
		cameraMode++
		cameraMode := Mod(cameraMode, totalCameraModes)
	}
	
	/*
	Cycles camera mode backward
	*/
	CycleCameraBackward()
	{
		global
		
		loopIters := totalCameraModes - 1
		
		; Hit the changeCamera key until we're one state behind our current state
		loop, %loopIters%
		{
			HotKeyPassthrough(OutputKeyMap.changeCamera)
		}
		
		cameraMode--
		cameraMode := Mod(cameraMode, totalCameraModes)
	}
}