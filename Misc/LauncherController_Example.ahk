
/*
Utilities class for the game launcher
*/
class LauncherController
{
	/*
	Default Constructor
	*/
	__New()
	{
		global

        doLaunch := false
        doAuthenticate := false
        
        /*
        !! REMEMBER TO REMOVE THESE BEFORE SHARING THIS SCRIPT WITH OTHERS !!
        */
        username    := ""
        pass        := ""
        /*
        !! REMEMBER TO REMOVE THESE BEFORE SHARING THIS SCRIPT WITH OTHERS !!
        */
    }
    
	/*
	Starts the game launcher
	*/
    Launch()
    {
        global
        
        ; Start the launcher and wait until it's active
        Run C:\Users\User\Documents\StarCitizen\Launcher\StarCitizenLauncher.exe
        WinWaitActive, Star Citizen Launcher

        ; If nothing went wrong
        if(!ErrorLevel)
        {
            ; Wait 9 seconds
            Sleep, 9000
            
            if(This.doAuthenticate)
            {
                ; Log in
                This.Authenticate()
            }
        }
    }
    
	/*
	Authenticates into the game launcher
	*/
    Authenticate()
    {
        global
        
        ; If the launcher isn't open
        IfWinNotExist, Star Citizen Launcher
        {
            ; Try to open it up
            This.Launch()
        }
        
        ; Try to set the launcher as the active window
        WinActivate, Star Citizen Launcher
        
        ; Wait until the launcher is the active window
        WinWaitActive, Star Citizen Launcher
        
        ; Enter the user credentials
        SendInput, {Tab}{Tab}%pass%{Enter}
    }
}