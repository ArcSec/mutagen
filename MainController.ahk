
/*
Global Settings
*/
	#SingleInstance force
	#Persistent
	#InstallKeybdHook
	#InstallMouseHook
	#UseHook On
/*
End Global Settings
*/




/*
I/O Settings0
*/
	SetKeyDelay, 10, 50, Play
	SetStoreCapslockMode, Off
/*
End I/O Settings
*/




/*
Includes
*/
	#Include Hotkeys/Hotkeys.ahk
	#Include Hotkeys/KeyMap.ahk
	#Include Ship Controllers/FlightModeController.ahk
	#Include CameraController.ahk
	#Include Misc/LauncherController.ahk
	#Include ModeController.ahk
/*
End Includes
*/




/*
Entry point for script suite
*/
Main()
{
	global
	
	; Set up the other controllers
	flightModeController := new FlightModeController()
	cameraController := new CameraController()
	launcherController := new LauncherController()
	modeController := new ModeController()
	
	; Set up hotkeys used by this controller
	Hotkey(InputKeyMap.exitScript, "KillController", "")
	Hotkey(InputKeyMap.toggleHotkeys, "ToggleHotKeys", "")
	Hotkey(InputKeyMap.toggleConsole, "HotKeyPassthrough", OutputKeyMap.toggleConsole)
	
	; Get command line parameters
	loop
	{
		switch := %a_index%
		
		if switch =
			break

		If switch = l
			launcherController.doLaunch := true

		If switch = a
			launcherController.doAuthenticate := true
	}

	; Handle command line parameters
	if(launcherController.doLaunch)
		launcherController.Launch()
	
	return
}

/*
Resets hardware state and terminates script suite
*/
KillController()
{
	; Reset hardware states
	SetCapsLockState, Off
	
	; Exit
	ExitApp, 1
}

/*
Resets hardware state and terminates script suite
*/
ToggleHotKeys()
{
	Suspend
}



/* 
Auto-Execute Block 
*/

	; Grant control to the main script loop
	Main()

	return
	
/* 
End Auto-Execute Block 
*/


/*
Labels
*/
	#Include Hotkeys/HandleHotkeys.ahk
/*
End Labels
*/