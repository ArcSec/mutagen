

/*
Set your keymap here.

This is the map of keys you will push
to trigger actions in this software.

!! THESE MUST ALL BE UNIQUE !!
*/
class InputKeyMap
{
	; TODO: Integrate an xml parser that can grab a user's keymaps automatically
	
	; Flight Mode
	static comStab 				:= "u" ; Toggles Command Stability
	static gSafe 				:= "i" ; Toggles G-force safeties
	static decouple 			:= "CapsLock" ; Toggles decoupled mode
	
	; Radar
	static zoomRadarOut			:= "," ; Decreases radar zoom factor
	static zoomRadarIn			:= "." ; Increases radar zoom factor
	static targetNearestHostile := "r" ; Targets nearest hostile
	static targetNextHostile	:= "t" ; Cycles through the hostiles list
	static targetNextContact	:= "y" ; Cycles through the contacts list
	static targetNextFriendly	:= "h" ; Cycles through the friendlies list
	static radarFocusBack		:= "k" ; Cycles the radar focus back
	static radarFocusFoward		:= "j" ; Cycles the radar focus forward
	static pinTarget			:= "g" ; Pins scanned target to the HUD
	
	; Misc
	static pilotEject			:= "{RAlt}l" ; Ejects pilot
	static cabinLights			:= "o" ; Toggles cabin lights
	static toggleConsole		:= "~" ; Opens and closes the game engine console
	
	; Camera
	static changeCamera			:= "Insert" ; Changes the camera's mode
	static cycleCameraForward	:= "p" ; Changes the camera's mode
	static cycleCameraBackward	:= "l" ; Changes the camera's mode
	
	; AHK
	static exitScript			:= "0" ; Kills script completely
	static toggleHotkeys		:= "9" ; Suspends/Resumes hotkeys
	static enableFPSMode		:= "8" ; Enables FPS Mode
	static enableFlightMode		:= "7" ; Enables Flight Mode
}



/*
Don't dick with these unless they're broken.

This is the map of keys the software will
send to the game.

These can be duplicates.
*/
class OutputKeyMap
{
	; TODO: Integrate an xml parser that can grab a user's keymaps automatically
	
	; Flight Mode
	static comStab 				:= "^{CapsLock}"
	static gSafe 				:= "^{CapsLock}"
	static decouple 			:= "{CapsLock}"
	
	; Radar
	static zoomRadarOut			:= ","
	static zoomRadarIn			:= ","
	static targetNearestHostile := "r"
	static targetNextHostile	:= "t"
	static targetNextContact	:= "y"
	static targetNextFriendly	:= "h"
	static radarFocusBack		:= "k"
	static radarFocusFoward		:= "j"
	static pinTarget			:= "g"
	
	; Misc
	static pilotEject			:= "{RAlt}l"
	static cabinLights			:= "o"
	static toggleConsole		:= "~"
	
	; Camera
	static changeCamera			:= "{Insert}"
}