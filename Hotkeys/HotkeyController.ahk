/*
Manages custom hotkeys
*/
class HotkeyController
{
	/*
	Default Constructor
	*/
	__New()
	{
		global
		
		Hotkeys := {}
	}

	/*
	Sets up a hotkey tied to a function.

	These hotkeys will work like normal hotkeys and don't require any special
	calling code.
	*/
	AddHotkey(customHotkey) 
	{
		global
		
		; Get input key
		key := customHotkey.Key
		
		; Add the hotkey to the array
		Hotkeys[key] := customHotkey
		
		; Set the hotkey
		ResumeHotkey(customHotkey)
	}

	/*
	Removes a hotkey
	*/
	RemoveHotkey(customHotkey)
	{
		global
		
		; Get input key
		key := customHotkey.Key
		
		; Remove the hotkey from the array
		Hotkeys[key] := ""
		
		; Unset the hotkey
		Hotkey, %key%, Passthrough
	}

	/*
	Resumes a hotkey
	*/
	ResumeHotkey(customHotkey)
	{
		global
		
		; Get input key
		key := customHotkey.Key
		
		; Set the hotkey
		Hotkey, %key%, SendHotkey
	}

	/*
	Pauses a hotkey
	*/
	SuspendHotkey(customHotkey)
	{
		global
		
		; Get input key
		key := customHotkey.Key
		
		; Pause the hotkey
		Hotkey, %key%, PauseHotkey
	}
}