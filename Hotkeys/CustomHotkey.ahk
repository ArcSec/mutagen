/*
Defines an extended hotkey
*/
class CustomHotkey
{
	/*
	Default Constructor
	*/
	__New()
	{
		global
		
		Key := ""
		Function := ""
		FunctionVars := ""
		
		Hold := false
		RepeatCount := 1
	}
	
	/*
	Use the hotkey
	*/
	Send(passthrough)
	{
		; If we only want to send the keystrokes
		if(passthrough = true)
		{
			; Suspend all hotkeys to avoid clashes and loops
			Suspend, On
			
			; Send our keystrokes through
			SendInput %Key%
			
			; Resume all hotkeys
			Suspend, Off
			
			return
		}
		
		
		if(RepeatCount = -1)
		{
			; Repeat until told otherwise
			loop
			{
				; Call our custom function
				Function(FunctionVars);
			}
		}
		else
		{
			; Repeat as many times as was specified
			loop, %RepeatCount%
			{
				; Call our custom function
				Function(FunctionVars);
			}
		}
	}
}