
Hotkeys := {}

/*
Sets up a hotkey tied to a function.

These hotkeys will work like normal hotkeys and don't require any special
calling code.
*/
HotKey(key, theFunction, theVars) 
{
    global 
	
	Hotkeys[key] := { Function:theFunction, Vars:theVars }
	Hotkey, %key%, HandleHotkey ; HandleHotkey is in it's own, self-titled script
}

/*
Sends a hotkey through without triggering other hotkeys
*/
HotKeyPassthrough(key)
{
	Suspend, On
	SendInput %key%
	Suspend, Off
}