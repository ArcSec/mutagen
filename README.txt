* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
*                                     /mhs+/-`                                                                          *
*                                     /MMMMMMMNdhs+:.`                                                                  *
*                                     /MMMMMMMMMMMMMMMNdyo+:`                                                           *
*                                     /MMMMMMMMMMMMMMMMMMMMh`.s.                                                        *
*                                     -dmmNNMMMMMMMMMMMMMMs -NMMs`                                                      *
*                                                          /MMMMMN+                                                     *
*                                    oNmmddhhhyyssoo++/-  oMMMMMMMMm/                                                   *
*                                  .dMMMMMMMMMMMMMMMMMd` hMMMMMMMMMMMm:                                                 *
*                                 +NMMMMMMMMMMMMMMMMMy .dMMMMMMMMMMMMMMd-                                               *
*                               .hMMMMMMMMMMMMMMMMMM+ -NMMMMMMMMMMMMMMMMMy.                                             *
*                              /NMMMMMMMMMMMMMMMMMN: /MMMMMMMMMMMMMMMMMMMMMs`                                           *
*                            `hMMMMMMMMMMMMMMMMMMm. oMMMMMMMMMMMMMMMMMMMMMMMNo`                                         *
*                           /NMMMMMMMMMMMMMMMMMMh` hMMMMMMMMMMMMMMMMMMMMMMMMMMN+   .-:://+oo+                   .:/`    *
*                         `yMMMMMMMMMMMMMMMMMMMs .dMMMMMMMMMMMMMMMMMMMMMMMMMMMMMm\ .yMMMMMMo              `-+sdNNo`     *
*                        :NMMMMMMMMMMMMMMMMMMM/ -NMMMMMMMMMMMMMMMMMMMMMMMMM+oNMMMMd: -hMMm-          ./ohmMMMMMy`       *
*                      `sMMMMMMMMMMMMMMMMMMMN- /MMMMMMMMMMMMMMMMMMMMMMMMMMMm. +NMMMMh- -o`     `-+sdNMMMMMMMMh-         *
*                     -mMMMMMMMMMMMMMMMMMMMd` sMMMMMMMMMMMMMMMMMMMMMMMMMMMMMN+  /NMMMMy.   /ohmMMMMMMMMMMMMm:           *
*                    sMMMMMMMMMMMMMMMMMMMMy `hMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNydMMMMMMMs` -dMMMMMMMMMMMMN+             *
*                  -mMMMMMMMMMMMMMMMMMMMMs  :hNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNo` :dMMMMMMMMMo`              *
*                 oMMMMMMMMMMMMMMMMMMMMMMMmy.  .+ymMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMN+  \mMMMMMy.                *
*               .dMMMMMMMMMMMMMMMMMMMMMMMMMMMMNy+. .\smMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMm\  +NMd-                  *
*              +MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNy+- `:sdMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMd- `-                    *
*            .dMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNh+- `:ohNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMh.                     *
*           +NMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNmdhyso+/:-.`      -+yNMMMMMMMMMMMMMMMMMMMMMMMMMMMMd+.                     *
*         `hMMMMMMMMMMMMMMMMMMMNmdhyso+/:-.`                        .\ymMMMMMMMMMMMMMMMMMMMMMh+`                        *
*        /NMMMMMMNmdhyso+/:-.`                                          .\sdMMMMMMMMMMMMMMh/`                           *
*       -+//:-.`                                                            `:odMMMMMMNy/`                              *
*                            .NMMN'                                             `-ohy:                                  *
*                     /++++oNMMMN/                                                                                      *
*                     .dMMMMMMMd.                                                                                       *
*                       hMMMMMh                                                                                         *
*                     `sMMMMMMMs`            `MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMN+  -MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMm       *
*                    -mMMMMdMMMMm-           `MMMMMMMMMMMMMMMMMMMMMMMMMMMMMd.   -MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMm       *
*                   +MMMMN: :NMMMMo           .......................-hMMN+     -MMM+........................hMMm       *
*                 .hMMMMh`   `hMMMMh.         ///////////////////////dMMh.      -MMM:                        +yys       *
*                /NMMMM+       +MMMMN/       `MMMMMMMMMMMMMMMMMMMMMMMMM+        -MMM:                                   *
*              `yMMMMd.         .dMMMMy`     `MMMNdddddddddddddddddddMMm-       -MMM:                                   *
*             -mMMMMs:::::::::::-:dMMMMm-    `MMMy                   -mMMs`     -MMM:                                   *
*            oMMMMm/yMMMMMMMMMMMMMMMMMMMMo   `MMMy                    `sMMm:    -MMMhssssssssssssssss:                  *
*          .dMMMMy:mMMMMMMMMMMMMMMMMMMMMMMd. `MMMh                      :mMMy`  -MMMMMMMMMMMMMMMMMMMMo                  *
*         /NMMMN/.ssssssssssssssyyyyyyyyyyyy.`syy+                       `oyso` .ssssssssssssyyyysyyy/                  *
*       `hMMMMd.                                                                                                        *
*      :NMMMMo   /mmsssssssso  dMyssssss/  /hdysssssss  Mm        NM  NMsssssssymh-  MN .ssssssssss. dm\    /md/        *
*     sMMMMm-    :dmyyyyyyyo:  dM.oooooo` oMo           Mm        NM  NM       `oMs  MN      NM       :hNo+md:          *
*   .mMMMMy`         ``````+M+ dM`        :Nh-          mM:      :Md  NM yyyydMMy    MN      NM         -NM:            *
*  /mNNNm/       :yyyyyyyyhy+` ydhyyyyyy/  `/shyyyyyyy  `/shyyyyhs/`  hh      .odo.  dh      hd          hd             *
*                                                                                                                       *
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* Title         : MutaGen                                                                                               *
* Link          : https://bitbucket.org/ArcSec/mutagen                                                                  *
* Authors       : Siegen                                                                                                *
* Organization  : Arc Security, http://arcsecurity.org                                                                  *
* Date          : 20140621                                                                                              *
* License       : (c) 2014 Arc Security, MIT License, http://opensource.org/licenses/MIT                                *
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

Description:

This is an input and state management suite for Star Citizen and its related services (such as the launcher). It's 
designed to help users configure the control mappings beyond the options available natively. It also keeps track of 
the states of various inputs (such as whether Decoupled mode is on or off) to allow for more intelligent key mappings
and macros.


Usage:

To use this suite, you will need to install AutoHotKey from ahkscript.org . To run the suite, execute 
MainController.ahk, typically by just double clicking it. To close the suite, the default keybind is "0", but that can 
be remapped, if desired.

To change the keybindings, change the values in /Hotkeys/KeyMap.ahk .