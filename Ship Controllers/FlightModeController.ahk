
/*
Controls the flight modes such as coupling,
G-Safeties, and Command Stability
*/
class FlightModeController
{
	/*
	Default Constructor
	*/
	__New()
	{
		global
		
		; Initial states
		gSafe := true
		comStab := true
		decouple := false
		
		; Hotkeys used by this controller
		Hotkey(InputKeyMap.gSafe, flightModeController.ToggleGSafe, "")
		Hotkey(InputKeyMap.comStab, flightModeController.ToggleComStab, "")
		Hotkey(InputKeyMap.decouple, flightModeController.ToggleDecouple, "")
	}

	/*
	Toggles the G-Safties on and off
	*/
	ToggleGSafe()
	{
		global
		
		if(gSafe)
		{
			; If the safty is alreay on, we only need to step down one state
			HotKeyPassthrough(OutputKeyMap.gSafe)
		}
		else
		{
			; Else, we need to wrap around to one state behind our
			; current position
			HotKeyPassthrough(OutputKeyMap.gSafe)
			HotKeyPassthrough(OutputKeyMap.gSafe)
			HotKeyPassthrough(OutputKeyMap.gSafe)
		}
		
		; Toggle the tracking variable
		gSafe := !gSafe
	}

	/*
	Toggles the Command Stability on and off
	*/
	ToggleComStab()
	{
		global
		
		; We need to step down two states to toggle
		HotKeyPassthrough(OutputKeyMap.comStab)
		HotKeyPassthrough(OutputKeyMap.comStab)
		
		; Toggle the tracking variable
		comStab := !comStab
	}


	/*
	Toggles the coupling on and off
	*/
	ToggleDecouple()
	{
		global
		
		; Pass the input through unaltered
		HotKeyPassthrough(OutputKeyMap.decouple)
		
		; Toggle the tracking variable
		decouple := !decouple
	}
}
